FROM node:11-alpine
WORKDIR /app

ADD package.json .
ADD package-lock.json .

RUN npm ci

ADD ./ /app

ENV PORT=5000
EXPOSE 5000

CMD node index
