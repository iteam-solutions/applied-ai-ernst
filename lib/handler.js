const fs = require('fs')

const handler = (req, res) => {
  if (req.method !== 'GET') {
    res.writeHead(500)
    res.end('nope')
  }
  
  if (!req.url || req.url === '/') {
    res.writeHead(200)
    return res.end('<a href="draw2.html">Draw</a> <a href="view.html">View</a>')
  }

  if (req.url) {
    res.writeHead(200)
    fs.readFile(__dirname + req.url, (error, data) => {
      if (error) {
        if (error.code === 'ENOENT') {
          res.writeHead(404)
          return res.end('Error 404: file not found.')
        }

        res.writeHead(500)
        return res.end('Error 500: everything broke.')
      }
      res.writeHead(200)
      res.end(data)
    })
  }
}

module.exports = handler
