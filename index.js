const handler = require('./lib/handler')
const server = require('http').createServer(handler)
const io = require('socket.io')(server)

server.listen(process.env.PORT || '9050')

io.on('connection', socket => {
  socket.on('onMouseDown', data => {
    console.log('> Mouse down', data)
    io.sockets.emit('onMouseDown', data)
  })
  
  socket.on('transfer', data => {
    console.log('> Mouse up', data)
    io.sockets.emit('transfer', data)
  })

  socket.on('transferComplete', data => {
    console.log('> Transfer complete', data)
    io.sockets.emit('transferComplete', data)
  })

  socket.on('clear', data => {
    console.log('> Clear canvas', data)
    io.sockets.emit('clear', data)
  })
})

console.log('Ernst 2.0 online')
