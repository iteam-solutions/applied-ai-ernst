# The AI that replaces Ernst Billgren

```bash
# Start a simple http server so you can access the drawing app
python3 -m http.server 9090
```

## TODO

- [x] Skapa en web socket relay @ilix
- [ ] Se till att ritande fungerar bra på iPad
- [ ] Ta bort scrollningen i iPad
- [ ] Fixa transfer/mouse up på iPad
- [x] Lyft in all html under express i samma repo som socket-relay
- [x] Clear-knapp (event)
- [ ] Helskärm vid ritning
- [ ] Helskärm vid rendering
- [ ] Sudda
